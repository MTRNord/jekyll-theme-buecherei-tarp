# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-theme-buecherei-tarp'
  spec.version       = '0.1.0'
  spec.authors       = ['MTRNord']
  spec.email         = ['info@nordgedanken.de']

  spec.summary       = 'A jekyll theme specific to the Bücherei in Tarp, Germany'
  spec.homepage      = 'https://buecherei.nordgedanken.dev'
  spec.license       = 'AGPLv-3.0'

  spec.metadata['plugin_type'] = 'theme'

  spec.files = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(assets|_(includes|layouts|sass)/|(LICENSE|README)(\.(txt|md|markdown)|$))}i)
  end

  spec.add_runtime_dependency 'jekyll', '~> 4.0'
  spec.add_runtime_dependency 'jekyll-pwa-plugin'
  spec.add_runtime_dependency 'jekyll-seo-tag'
  spec.add_runtime_dependency 'jekyll-target-blank'

  spec.add_development_dependency 'bundler', '>= 1.16'
  spec.add_development_dependency 'rake', '~> 12.0'
end
